## PROJET ASSOCIATION TRANS MUSICAL (ATM)

#### *IUT Informatique Lannion*
*__Théo GUILLOSSOU__*   
*__Elouan PETEREAU__*

---

### A propos

Ce fichier permet de connaître les différents fichiers présents sur le git, il existe deux répertoires :
 - le dossier ```/WEBSITE``` contient le site web
 - le dossier ```/DATABASE``` contient les scripts SQL pour la création de la base de donnée et les fichiers CSV nécessaires pour peupler la base


---

### Création et Peuplage de la base
Pour créer la base de donnée il suffit de suivre les étapes suivantes
1. exécuter le fichier SQL ATM_BASE_table.sql
2. exécuter le fichier SQL ATM_ASSO_table.sql
3. exécuter le fichier SQL ATM_ASSO_table_populate.sql
4. exécuter le fichier SQL ATM_import_csv.sql
    > si l'execution de ce fichier ne fonctionne pas il faut importer les fichiers csv à la main
5. exécuter le fichier SQL ATM_ASSO_populate.sql

__/!\ Il faut garder l'emplacement du dossier__ ```\CSV``` __dans la racine du dossier contenant les fichiers SQL /!\\__



******
### Informations concernant les tables de la base


__Listes des tables :__
- __Pays:__ Un pays comprend un id et un nom et représente l'ensemble des pays utilisés dans la base.

- __Ville:__ Une ville possède un id, un nom et un id de pays unique.
- __Lieu:__ Un lieu comprend un id, un nom, un entier représentant le booléen pour l'acces handicapé du lieu, et un nombre de places, il possède également un identifiant de type de lieu et de ville.
- __Typelieu:__ Un type de lieu possède un id et une description. Cette table permet de regrouper l'ensemble des types de lieu couvert par les Trans à savoir aujourd'hui les bars et salles de concert.
- __Groupe:__ Un groupe possède un id, un nom ainsi qu'un genre musicale. Plusieurs artistes peuvent appartenir à un même groupe.
- __Artiste:__ Une artiste possède un id et un nom. Il peut appartenir à un ou plusieurs groupe. Dans le cas où l'artiste n'a pas de groupe, un groupe portant son nom sera créé.
- __Compte:__ Un compte possède un id, un nom d'utilisateur, un mot de passe, un mail, un niveau de droit et un état. Un niveau de droit sera attribué à 2 (1 pour droit administrateur et 2 pour utilisateur) par défaut. Un compte peut-être lié à un artiste  mais pas nécessairement (besoin de compte administrateur et autres...).
    Les états disponibles sont les suivants :

    |nom état|entier représentation|
    |---|:---:|
    |validé|1|
    |en cours de validation|2|
    |banis|3|

- __Date:__ Une date contient un id et une date associée, l'ensemble des dates utilisées sont gérées par cette table
- __Soiree:__ Une soirée contient un id, un nom et une durée. Il s'agit d'un événement à thème comme par exemple une soirée jazz et qui peut avoir lieu à l'interieur d'un projet ou directement dans le festival sans passer par un projet.
- __Projet:__ Un projet contient un id et un nom, il s'agit d'une sorte de mini festival interne au festival comme par exemple le projet "Bar en Trans" dans le festival "Transmusicales de Rennes"
- __Festival:__ Un festival possède un id et un nom et représente une édition du festival comme par exemple la 12ème édition du festival des Transmusicales de Rennes
- __Concert:__ Un concert possède un id, un nom et une durée et représente seulement la prestation préparé de l'artiste.
    Ainsi, c'est à l'artiste de choisir si sa représentation va changer ce qui fait qu'un même concert peut être joué plusieurs fois et ce à des dates différentes et dans des lieux différents.
- __Reservation:__ Une réservation comprends un id et un etat de reservation. Cet état est représenté par un entier et permet de savoir si il y a une demande de réservation et si elle est en cours d'acceptation ou non. De plus afin de garder un historique des réservations passés nosu avons créés un état reservation terminé.
    Ainsi plusieurs états existents et sont les suivants : 

    |nom état|entier représentation|
    |---|:---:|
    |libre|1|
    |reservé|2|
    |en cours de traitement|3|
    |terminé|4|

    Un reservation est un élément faisant la liaison entre un concert, une date, un lieu, et un projet ou une soirée. La réservation permet d'établir un planning de ce qui sera joué alors qu'un concert permettra seulement de savoir ce que va jouer l'artiste sans date, sans lieu et sans projet/soirée.