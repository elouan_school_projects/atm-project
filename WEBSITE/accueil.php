<!DOCTYPE html>
<html lang="fr">

<head>
    <title>ATM Accueil</title>
    <meta charset="UTF-8">
    <meta name="description" content="Page d'accueil de l'intranet d'ATM">
    <meta name="keywords" content="Association Trans Musicale, ATM">
    <meta name="author" content="Elouan PETEREAU">
    <meta name="author" content="Théo GUILLOUSOU">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="style.css" />
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="bootstrap-4.1.3-dist/js/bootstrap.js"></script>
</head>

<body>
    <section id="connexion">
        <form class="form-signin" action="recherche.php">
            <h2 class="form-heading">Connexion</h2>
            <label for="inputEmail" class="sr-only">Nom de compte</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Nom de compte" required autofocus>
            <label for="inputPassword" class="sr-only">Mot de passe</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Se souvenir de moi
                </label>
            </div>
            <div class="button">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
            </div>
        </form>
        <form class="form-register" action="inscription.php">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Inscription</button>
        </form>


    </section>
</body>

</html>