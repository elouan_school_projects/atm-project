<?php
include("connexion.php");
include("functions.php");

$artistName= "… and youll know us by the trail of dead";
$dbh = connectDB($serveur, $base, $id, $mdp);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>ATM Inscription</title>
    <meta charset="UTF-8">
    <meta name="description" content="Page d'inscription de l'intranet d'ATM">
    <meta name="keywords" content="Association Trans Musicale, ATM">
    <meta name="author" content="Elouan PETEREAU">
    <meta name="author" content="Théo GUILLOUSOU">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="style.css" />
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="bootstrap-4.1.3-dist/js/bootstrap.js"></script>
</head>

<body>
    <header>
        <nav class="navbar navbar-inverse navbar-static-top navbar-dark bg-dark shadow d-lg-none" role="navigation">
            <a class="navbar-brand" href="accueil.php">Association Trans Musicales</a>
            <div class="icon_burgerMenu navbar-toggle collapsed" data-toggle="collapse" data-target="#toggleNav"
                onclick="menuToggle(this)">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
            <div class="collapse navbar-collapse" id="toggleNav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="recherche.php">Rechercher/Reserver une salle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="mes_reservations.php">Afficher mes réservations</a>
                    </li>
                    <li class="nav-item text-nowrap active">
                        <a class="nav-link" href="accueil.php">Sign out</a>
                    </li>
                </ul>
            </div>

        </nav>
        <nav class="navbar navbar-dark bg-dark shadow d-none d-lg-flex">
            <div id="navabar_content">
                <a class="navbar-brand" href="accueil.php">Association Trans Musicales</a>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="recherche.php">Rechercher/Reserver une salle</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="mes_reservations.php">Afficher mes réservations</a>
                    </li>
                </ul>
            </div>
            <ul class="nav navbar-nav">
                <li class="nav-item text-nowrap active">
                    <a class="nav-link" href="accueil.php">Sign out</a>
                </li>
            </ul>
        </nav>
    </header>
    <section id="reservation">

        <form class="form-book" method="post" action="">
            <h2 class="form-heading">Réserver une salle</h2>
            <fieldset>
                <legend>Nom du concert :</legend>
                <label for="input_concertName" class="sr-only">Nom du concert</label>
                <select id="inputConcertName" class="custom-select" name="concertName" placeholder="Concert" required>
                    <option selected disabled>Selectionnez un concert...</option>
                    <?php
                        createConcertList(getConcertList($dbh));
                        ?>
                </select>
            </fieldset>
            <fieldset>
                <legend>Date de la réservation :</legend>
                <label for="input_dateSPicker">Date de début :</label>
                <input id="input_dateSPicker" class="form-control" type="datetime-local" name="dateS" placeholder="aaaa-mm-jjT:hh:mm"
                    required>
                <label for="input_dateEPicker">Date de fin :</label>
                <input id="input_dateEPicker" class="form-control" type="datetime-local" name="dateE" placeholder="aaaa-mm-jjT:hh:mm"
                    required>
            </fieldset>
            <div class="button">
            <?php
                echo("<button class=\"btn btn-lg btn-primary btn-block\" name=\"bookDone\" value=\"$_POST[book]\" type=\"submit\">Valider</button>");
            ?>
            </div>
        </form>

    </section>

    <div class="modal fade" id="book_Alert" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Attention</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Erreur dans la transmission du nom de la salle veuillez recharger la page</p>
                    <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="book_ok_Alert" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Merci</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Votre salle à été réservé</p>
                    <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</body>
<?php
    if(isset($_POST['book'])) {
    } else if (isset($_POST['bookDone'])) {
        if (isset($_POST['concertName'])&& isset($_POST['dateS'])&& isset($_POST['dateE'])) {
            book($dbh, $artistName, $_POST['bookDone'], $_POST['dateS'], $_POST['dateE'], $_POST['concertName']);
            echo("
            <SCRIPT LANGUAGE=JavaScript>
                 $('#book_ok_Alert').modal('show')
            </SCRIPT>
        ");
        }
    } else {
        echo("
            <SCRIPT LANGUAGE=JavaScript>
                 $('#book_Alert').modal('show')
            </SCRIPT>
        ");
    }
    //$_POST = array();
?>

</html>