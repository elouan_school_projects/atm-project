<?php
include("connexion.php");
include("functions.php");

$dbh = connectDB($serveur, $base, $id, $mdp);
$GLOBALS['searchResult'] = [];
$artistName= "… and youll know us by the trail of dead";
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>ATM Inscription</title>
    <meta charset="UTF-8">
    <meta name="description" content="Page d'inscription de l'intranet d'ATM">
    <meta name="keywords" content="Association Trans Musicale, ATM">
    <meta name="author" content="Elouan PETEREAU">
    <meta name="author" content="Théo GUILLOUSOU">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="style.css" />
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="bootstrap-4.1.3-dist/js/bootstrap.js"></script>
</head>

<body>
    <header>
        <nav class="navbar navbar-inverse navbar-static-top navbar-dark bg-dark shadow d-lg-none" role="navigation">
            <a class="navbar-brand" href="accueil.php">Association Trans Musicales</a>
            <div class="icon_burgerMenu navbar-toggle collapsed" data-toggle="collapse" data-target="#toggleNav"
                onclick="menuToggle(this)">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
            <div class="collapse navbar-collapse" id="toggleNav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="recherche.php">Rechercher/Reserver une salle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="mes_reservations.php">Afficher mes réservations</a>
                    </li>
                    <li class="nav-item text-nowrap active">
                        <a class="nav-link" href="accueil.php">Sign out</a>
                    </li>
                </ul>
            </div>

        </nav>
        <nav class="navbar navbar-dark bg-dark shadow d-none d-lg-flex">
            <div id="navabar_content">
                <a class="navbar-brand" href="accueil.php">Association Trans Musicales</a>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="recherche.php">Rechercher/Reserver une salle</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="mes_reservations.php">Afficher mes réservations</a>
                    </li>
                </ul>
            </div>
            <ul class="nav navbar-nav">
                <li class="nav-item text-nowrap active">
                    <a class="nav-link" href="accueil.php">Sign out</a>
                </li>
            </ul>
        </nav>
    </header>
    <section id="mes_reservation">
        <h2 class="form-heading">Liste de mes Réservation</h2>
        <?php
            $GLOBALS['searchResult'] = searchForBooklist($dbh, $artistName );
            createTable($GLOBALS['searchResult']);
        ?>
    </section>

</body>

</html>