<?php
include("connexion.php");
include("functions.php");

$GLOBALS['searchResult'] = [];
$dbh = connectDB($serveur, $base, $id, $mdp);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>ATM Recherche d'un lieu</title>
    <meta charset="UTF-8">
    <meta name="description" content="Page de recherche de l'intranet d'ATM">
    <meta name="keywords" content="Association Trans Musicale, ATM, recherche, lieu">
    <meta name="author" content="Elouan PETEREAU">
    <meta name="author" content="Théo GUILLOUSOU">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="style.css" />
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="bootstrap-4.1.3-dist/js/bootstrap.js"></script>
</head>

<body>

<header>
        <nav class="navbar navbar-inverse navbar-static-top navbar-dark bg-dark shadow d-lg-none" role="navigation">
            <a class="navbar-brand" href="accueil.php">Association Trans Musicales</a>
            <div class="icon_burgerMenu navbar-toggle collapsed" data-toggle="collapse" data-target="#toggleNav"
                onclick="menuToggle(this)">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
            <div class="collapse navbar-collapse" id="toggleNav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="recherche.php">Rechercher une salle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="mes_reservations.php">Afficher mes réservations</a>
                    </li>
                    <li class="nav-item text-nowrap active">
                        <a class="nav-link" href="accueil.php">Sign out</a>
                    </li>
                </ul>
            </div>

        </nav>
        <nav class="navbar navbar-dark bg-dark shadow d-none d-lg-flex">
            <div id="navabar_content">
                <a class="navbar-brand" href="accueil.php">Association Trans Musicales</a>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="recherche.php">Rechercher une salle</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="mes_reservations.php">Afficher mes réservations</a>
                    </li>
                </ul>
            </div>
            <ul class="nav navbar-nav">
                <li class="nav-item text-nowrap active">
                    <a class="nav-link" href="accueil.php">Sign out</a>
                </li>
            </ul>
        </nav>
    </header>

    <section id="searchBar_l" class="d-none d-lg-flex">
        <h2>Salle Disponibles</h2>
        <form id="searchBar_form_l" class="form-inline" name="recherche" method="post" action="recherche.php">
            <div class="form-group row">
                <label for="input_cityPicker">Entrez une ville :</label>
                <select id="input_cityPicker" class="custom-select" name="city" placeholder="Ville" required>
                    <option selected disabled>Selectionnez une ville...</option>
                    <?php
                        createCityList(getCityList($dbh));
                        ?>
                </select>
                <label for="input_datePicker">Entrez une date :</label>
                <input id="input_datePicker" class="form-control" type="datetime-local" name="date" placeholder="aaaa-mm-jjT:hh:mm"
                    required>
                <button class="btn btn-primary" type="submit" name="valider" value="chercher">Chercher</button>
            </div>

            <div>
                <button class="btn btn-link button-close" type="button" data-toggle="collapse" data-target="#moreOptions"
                    aria-expanded="false">
                    &#8964 Plus d'options
                </button>
            </div>
            <div id="moreOptions" class="form-inline collapse">
                <div class="form-check-inline">
                    <label for="input_disableChecker">Accès handicapé :</label>
                    <input id="input_disableChecker" type="checkbox" value="remember-me">
                </div>
                <label for="input_quantityPicker">Nombre de places :</label>
                <input id="input_quantityPicker" class="form-control" type="number" name="quantity" min="1">
            </div>

        </form>
    </section>

    <section id="searchBar_s" class="d-lg-none">
        <h2>Salle Disponibles</h2>
        <form id="searchBar_form_s" name="recherche" method="post" action="recherche.php">
            <div class="form-group row">
                <label for="input_cityPicker">Entrez une ville :</label>
                <select id="input_cityPicker" class="custom-select" name="city" placeholder="Ville" required>
                    <option selected disabled>Selectionnez une ville...</option>
                    <?php
                        createCityList(getCityList($dbh));
                                                ?>
                </select>
                <label for="input_datePicker">Entrez une date :</label>
                <input id="input_datePicker" class="form-control" type="datetime-local" name="date" placeholder="aaaa-mm-jjT:hh:mm"
                    required>
                <button class="btn btn-primary" type="submit" name="valider" value="chercher">Chercher</button>
            </div>

            <div>
                <button class="btn btn-link button-close" type="button" data-toggle="collapse" data-target="#moreOptions"
                    aria-expanded="false">
                    &#8964 Plus d'options
                </button>
            </div>
            <div id="moreOptions" class="form-inline collapse">
                <div class="form-check-inline">
                    <label for="input_disableChecker">Accès handicapé :</label>
                    <input id="input_disableChecker" type="checkbox" value="remember-me">
                </div>
                <label for="input_quantityPicker">Nombre de places :</label>
                <input id="input_quantityPicker" class="form-control" type="number" name="quantity" min="1">
            </div>
        </form>
    </section>

    <section id="content">
        <?php
                if (isset($_POST['city']) && isset($_POST['date'])) {
                    $GLOBALS['searchResult'] = searchForPlace($dbh, $_POST['date'], $_POST['city']);
                    createCardList($GLOBALS['searchResult']);
                }
            ?>
        <form name="reservation" method="post" action="reservation.php">
            <button class="btn btn-primary" type="submit" name="book" value="prout">Réserver</button>
        </form>
    </section>
</body>

</html>