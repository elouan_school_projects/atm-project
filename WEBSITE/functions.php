<?php
$GLOBALS['request_search'] = '
select ATM._lieu.nom,acces_handi,nombre_place from ATM._lieu
inner join ATM._ville on ville_id=ATM._ville.id
where lower(ATM._ville.nom) like lower(?)

except

select ATM._lieu.nom,acces_handi,nombre_place from ATM._reservation
inner join ATM._date on date_deb_id=ATM._date.id
inner join ATM._date as d on date_fin_id=d.id
inner join ATM._lieu on ATM._lieu.id=lieu_id
inner join ATM._ville on ville_id=ATM._ville.id
where date (?) between ATM._date.date and d.date
and etat=2
order by nom;
';

$GLOBALS['request_searchBookList'] = '
select ATM._concert.nom as concertName,ATM._date.date as startDate,fd.date as finishDate,ATM._lieu.nom as roomName from ATM._artiste
inner join ATM._membre_artiste_groupe on ATM._artiste.id=id_artiste
inner join ATM._joue_groupe_concert on ATM._membre_artiste_groupe.id_groupe=ATM._joue_groupe_concert.id_groupe
inner join ATM._concert on ATM._joue_groupe_concert.id_concert=_concert.id
inner join ATM._reservation on ATM._reservation.concert_id=ATM._concert.id
inner join ATM._date on ATM._reservation.date_deb_id=ATM._date.id
inner join ATM._date as fd on ATM._reservation.date_fin_id=fd.id
inner join ATM._lieu on ATM._reservation.lieu_id=ATM._lieu.id
where ATM._artiste.nom=?
';

$GLOBALS['request_searchCity'] = 'SELECT nom FROM ATM._ville;';
$GLOBALS['request_insertAccount'] = 'INSERT INTO ATM._COMPTE(nom_utilisateur,mot_de_passe,mail,niveau_droit,etat) VALUES(?,?,?,2,2)';

$GLOBALS['UNKNOWN'] = "?";
$GLOBALS['request_searchCity'] = 'SELECT nom FROM ATM._ville;';
$GLOBALS['request_insertDate']='INSERT INTO ATM._DATE VALUES(?,?)';
$GLOBALS['request_searchConcert'] = 'select nom from ATM._concert;';

/* FUNCTIONS TO CONNECT TO DATABASE */
function connectDB($serveur, $base, $id, $mdp)
{
    try {
        $dbh= new PDO("pgsql:host=$serveur;dbname=$base", "$id", "$mdp");
        //echo"Connexion reussie\n";
        return $dbh;
    } catch (PDOException$e) {
        echo"Echec de connexion: ".$e->getMessage();
    }
}

/* FUNCTIONS TO GET THE CITY LIST */
function getCityList($dbh)
{
    $ans = $dbh->prepare($GLOBALS['request_searchCity']);
    $ans ->execute();
    $ans = $ans->fetchAll(PDO::FETCH_ASSOC);
    
    echo($ans);
    return $ans;
}

/* FUNCTIONS TO CREATE CITY LIST */
function createCityList($cityList)
{
    if ($cityList == null) {
        echo"<option>Aucune ville disponibles</option>";
    } else {
        foreach ($cityList as $list_val) {
            foreach ($list_val as $key => $val) {
                if ($key=="nom") {
                    echo" <option>$val</option>";
                }
            }
        }
    }
}


/* FUNCTIONS TO SEARCH FOR AVAILABLE PLACES */
function searchForPlace($dbh, $date, $place)
{
    $ans = $dbh->prepare($GLOBALS['request_search']);
    $ans ->execute(array("%$place%","$date"));
    $ans = $ans->fetchAll(PDO::FETCH_ASSOC);
    
    //print_r($ans);
    return $ans;
}

/* FUNCTIONS TO INSERT NEW ACCOUNT IN DATABASE */
function insertAccount($dbh, $pass, $username, $email)
{

    $res = $dbh->prepare($GLOBALS['request_insertAccount']);
    $res ->execute(array($username, $pass, $email));
}

/* FUNCTIONS TO CREATE CARD LIST */
function createCardList($placeList)
{
    if ($placeList == null) {
        echo"<h3>Aucune salle disponible</h3>";
    } else {
        foreach ($placeList as $list_val) {
            $name = $GLOBALS['UNKNOWN'];
            $access = $GLOBALS['UNKNOWN'];
            $seat = $GLOBALS['UNKNOWN'];
            foreach ($list_val as $key => $val) {
                if ($key=="nom") {
                    $name = $val;
                }
                if ($key=="acces_hand") {
                    $access = $val;
                }
                if ($key=="nb_places") {
                    $seat = $val;
                }
            }
            createCard($name, $access, $seat);
        }
    }
};


/* FUNCTIONS TO CREATE A CARD */
function createCard($name, $access, $seat)
{
    echo"  
            <div class=\"card\" style=\"width: 18rem;\">
                <div class=\"card-body\">
                    <h3 class=\"card-title\">$name</h3>
                    <table class=\"table table-sm\">
                        <tbody>
                            <tr>
                                <td>acces handicapé</td>
                                <td>$access</td>
                            </tr>
                            <tr>
                                <td>nombre places</td>
                                <td>$seat</td>
                            </tr>
                        </tbody>
                    </table>
                    <form name=\"reservation\" method=\"post\" action=\"reservation.php\">
                        <button class=\"btn btn-primary\" type=\"submit\" name=\"book\" value=\"$name\">Réserver</button>
                    </form>
                </div>
            </div>
        ";
};


function getUSers($dbh){
    $users=array();
    $getUsersReq="select * from atm._compte";
    foreach($dbh->query($getUsersReq) as $user){
        $users[]=$user['nom_utilisateur'];
    }
    return $users;
}

function getUSersMail($dbh){
    $mails=array();
    $getMailsReq="select * from atm._compte";
    foreach($dbh->query($getMailsReq) as $mail){
        $mails[]=$mail['mail'];
    }
    return $mails;
}

//GET GROUPE

function getGroupFormArtist($username,$dbh){
    $artisteGroupeNames=array();
    $artistId=getUSerId($dbh,$username)[0];
    $getArtisteGroupeNamesReq="select _groupe.nom from _artiste
    inner join ATM._membre_artiste_groupe on id_artiste=_artiste.id
    inner join _groupe on id_groupe=_groupe.id
    where _artiste.id=$artistId";

    foreach($dbh->query($getArtisteGroupeNamesReq) as $name){
        $artisteGroupeNames[]=$name['nom'];
    }
    return $artisteGroupeNames;
}

//get concertID

function getConcertId($dbh,$concertName){
    $concertId=array();
    $ans = $dbh->prepare("select * from ATM._concert where nom=?");
    $ans ->execute(array("$concertName"));
    $ans = $ans->fetchAll(PDO::FETCH_ASSOC);
    foreach($ans as $concert){
        $concertId[]=$concert['id'];
    }
    return $concertId[0];
}
//get room id 
function getRoomId($dbh,$roomName){
    $roomId=array();
   // $getRoomIdReq="select * from ATM._lieu where nom='?'";
    $ans = $dbh->prepare("select * from ATM._lieu where nom=?");
    $ans ->execute(array("$roomName"));
    $ans = $ans->fetchAll(PDO::FETCH_ASSOC);

    foreach($ans as $room){
        $roomId[]=$room['id'];
    }
    return $roomId[0];
}

//get date

function getAllDates($dbh){
    $dates=array();
    $getDatessReq="select * from atm._date";
    foreach($dbh->query($getDatessReq) as $date){
        $dates[]=$date['date'];
    }
    return $dates;
}

//get date id

function getDateId($dbh,$dateName){
    $dateId=array();
    $ans = $dbh->prepare("select * from ATM._date where date=?");
    //print_r($dateName."  ");
    $ans ->execute(array("$dateName"));
    $ans = $ans->fetchAll(PDO::FETCH_ASSOC);

    foreach($ans as $date){
        $dateId[]=$date['id'];
    }
    return $dateId[0];
}

function getMaxIdResa($dbh){
    $idsresa=array();
    $getidsresa="select max(id) as max from ATM._reservation";
    foreach($dbh->query($getidsresa) as $id){
        $idsresa[]=$id['max'];
    }
    return $idsresa[0];
}
function getMaxIdDate($dbh){
    $idsdate=array();
    $getidsdate="select max(id) as max from ATM._date";
    foreach($dbh->query($getidsdate) as $id){
        $idsdate[]=$id['max'];
    }
    return $idsdate[0];
}

function book($dbh,$artistName, $roomName, $startDate, $finishDate, $concertName){
    $concertId=getConcertId($dbh,$concertName);
    //$roomName="Aire libre";
    $roomId=getRoomId($dbh,$roomName);
    
    $startDate=date('Y-m-d h:m:s+01',strtotime($startDate));
    $finishDate=date('Y-m-d h:m:s+01',strtotime($finishDate));
    //print_r($startDate);
    $isStartDatePresent=in_array($startDate,getAllDates($dbh));
    $isFinishDatePresent=in_array($finishDate,getAllDates($dbh));
    // $test=getMaxIdDate($dbh)+1;
    // print_r($test);
    if($isStartDatePresent){
        $startDateId=getDateId($dbh,$startDate);
        //print_r($startDateId."  ");
    }else{ 
       // print_r($startDate);
        $ins =$dbh->prepare($GLOBALS['request_insertDate']);
        $ins ->execute(array(getMaxIdDate($dbh)+1,$startDate));
        $startDateId=getDateId($dbh,$startDate);
        //print_r($startDateId."  ");
    }

    if($isFinishDatePresent){
        $finishDateId=getDateId($dbh,$finishDate);
        //print_r($finishDateId."  ");
    }else{
        $ins =$dbh->prepare($GLOBALS['request_insertDate']);
        $ins ->execute(array(getMaxIdDate($dbh)+1,$finishDate));
        $finishDateId=getDateId($dbh,$finishDate);
        //print_r($finishDateId."  ");
    }
    $id=getMaxIdResa($dbh);
    $id++;
   // print_r("id:".$id." concert:".$concertId." startdate".$startDateId." finisdate".$finishDateId." room".$roomName."= ".$roomId);
    $requestInsertResaInsert ="INSERT INTO ATM._reservation(id,etat,concert_id,date_deb_id,date_fin_id,lieu_id) VALUES(?,2,?,?,?,?)";
    $insResa =$dbh->prepare($requestInsertResaInsert);
    $insResa ->execute(array($id,$concertId,$startDateId,$finishDateId,$roomId));  
}

/* FUNCTIONS TO CREATE A TABLE LINE FROM A BOOK */

function createTableLine($concertName, $roomName, $startDate, $finishDate){
    echo"
    <tr>
        <th scope=\"row\">$concertName</th>
        <td>$roomName</td>
        <td>$startDate</td>
        <td>$finishDate</td>
    </tr>
    ";
}

/* FUNCTIONS TO CREATE TABLE FROM A BOOK LIST*/

function createTable($bookList){
    if ($bookList == null) {
        echo"<h3 class=\"text-center\">Aucune reservations</h3>";
    } else {
        echo"<table class=\"table\">
                <thead class=\"text-light\">
                    <tr class=\"bg-primary\">
                        <th scope=\"col\">Nom</th>
                        <th scope=\"col\">Salle</th>
                        <th scope=\"col\">Date de début</th>
                        <th scope=\"col\">Date de fin</th>
                   </tr>
                </thead>
                <tbody>";

        foreach ($bookList as $list_val) {
            foreach ($list_val as $key => $val) {
                if ($key=="concertname") {
                    $concertName = $val;
                    //print_r($val);
                }
                if ($key=="roomname") {
                    $roomName = $val;
                }
                if ($key=="startdate") {
                    $startDate = $val;
                }
                if ($key=="finishdate") {
                    $finishDate = $val;
                }
            }
            createTableLine($concertName, $roomName, $startDate, $finishDate);
        }
        echo"</tbody></table>";
    }
}

/* FUNCTIONS TO SEARCH FOR CURRENT BOOKLIST */
function searchForBooklist($dbh,$artistName)
{
    $ans = $dbh->prepare($GLOBALS['request_searchBookList']);
    $ans ->execute(array("$artistName"));
    $ans = $ans->fetchAll(PDO::FETCH_ASSOC);
    
    //print_r($ans);
    return $ans;
}

/* FUNCTIONS TO GET THE CONCERT LIST */
function getConcertList($dbh)
{
    $ans = $dbh->prepare($GLOBALS['request_searchConcert']);
    $ans ->execute();
    $ans = $ans->fetchAll(PDO::FETCH_ASSOC);
    
    echo($ans);
    return $ans;
}

/* FUNCTIONS TO CREATE CONCERT LIST */
function createConcertList($concertList)
{
    if ($concertList == null) {
        echo"<option>Aucun concert disponibles</option>";
    } else {
        foreach ($concertList as $list_val) {
            foreach ($list_val as $key => $val) {
                if ($key=="nom") {
                    echo" <option>$val</option>";
                }
            }
        }
    }
}
