<?php
include("connexion.php");
include("functions.php");

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>ATM Inscription</title>
    <meta charset="UTF-8">
    <meta name="description" content="Page d'inscription de l'intranet d'ATM">
    <meta name="keywords" content="Association Trans Musicale, ATM">
    <meta name="author" content="Elouan PETEREAU">
    <meta name="author" content="Théo GUILLOUSOU">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="style.css" />
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <script src="bootstrap-4.1.3-dist/js/bootstrap.js"></script>
</head>

<body>
    <section id="connexion">

        <form class="form-register" method="post" action="">
            <h2 class="form-heading">Inscription</h2>
            <label for="inputEmail" class="sr-only">Nom de compte</label>
            <input type="text" name="username" id="inputEmail" class="form-control" placeholder="Nom de compte"
                required autofocus>
            <label for="inputEmail" class="sr-only">E-mail</label>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="E-mail" required>
            <label for="inputPassword" class="sr-only">Mot de passe</label>
            <input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Mot de passe"
                required>
            <label for="inputPasswordConf" class="sr-only">Confirmation mot de passe</label>
            <input type="password" name="passConf" id="inputPasswordConf" class="form-control" placeholder="Confirmation mot de passe"
                required>
            <div class="button">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Valider</button>
            </div>
        </form>
        <form>

        <div class="modal fade" id="passwordError_Alert" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Erreur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Les mots de passe entrés ne correspondent pas veuillez réessayer.</p>
                        <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="sameError_Alert" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Erreur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Le nom d'utilisateur ou mail existe déjà.</p>
                        <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="registerValid_Alert" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Merci</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Votre demande d'inscription est prise en compte et sera traitée le plus rapidement possible.</p>
                        <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<?php
    $dbh = connectDB($serveur, $base, $id, $mdp);
    $users=getUsers($dbh);
    if (isset($_POST['pass']) && isset($_POST['passConf']) && $_POST['pass']!=$_POST['passConf']) {
        echo("
            <SCRIPT LANGUAGE=JavaScript>
                 $('#passwordError_Alert').modal('show')
            </SCRIPT>
        ");
    } 
    
    else if (isset($_POST['username']) && isset($_POST['pass']) && isset($_POST['email'])) {
        $ispresnt=in_array($_POST['username'],getUsers($dbh));
        $ispresntmail=in_array($_POST['email'],getUsersMail($dbh));
        if($ispresnt){
            echo("
            <SCRIPT LANGUAGE=JavaScript>
                 $('#sameError_Alert').modal('show')
            </SCRIPT>
        ");

        }else if($ispresntmail){
                echo("
                <SCRIPT LANGUAGE=JavaScript>
                     $('#sameError_Alert').modal('show')
                </SCRIPT>
            ");
        }
        
        else{
            insertAccount($dbh, crypt($_POST['pass']), $_POST['username'], $_POST['email']);
            echo("
            <SCRIPT LANGUAGE=JavaScript>
                $('#registerValid_Alert').modal('show')
            </SCRIPT>
            ");
        }
            
    }
    
   

    $_POST = array();
?>
</html>
