SET SCHEMA 'ATM';

CREATE TABLE ATM._ORIGINE_VILLE_GROUPE (
 id_ville             INTEGER,
 id_groupe            INTEGER
);
alter table  ATM._ORIGINE_VILLE_GROUPE 
add CONSTRAINT _origine_ville_groupe_pk PRIMARY KEY (id_ville,id_groupe),
add CONSTRAINT _origine_ville_groupe_fk1 FOREIGN KEY (id_groupe) REFERENCES ATM._GROUPE(id),
add CONSTRAINT _origine_ville_groupe_fk2 FOREIGN KEY (id_ville) REFERENCES ATM._VILLE(id);



CREATE TABLE ATM._MEMBRE_ARTISTE_GROUPE (
 id_artiste           INTEGER,
 id_groupe            INTEGER
);
alter table ATM._MEMBRE_ARTISTE_GROUPE
add CONSTRAINT _membre_artiste_groupe_pk PRIMARY KEY (id_artiste,id_groupe),
add CONSTRAINT _membre_artiste_groupe_fk1 FOREIGN KEY (id_groupe) REFERENCES ATM._GROUPE(id),
add CONSTRAINT _membre_artiste_groupe_fk2 FOREIGN KEY (id_artiste) REFERENCES ATM._ARTISTE(id);

CREATE TABLE ATM._JOUE_GROUPE_CONCERT (
 id_groupe            INTEGER,
 id_concert           INTEGER
);
alter table ATM._JOUE_GROUPE_CONCERT
add CONSTRAINT _joue_groupe_concert_pk PRIMARY KEY (id_groupe,id_concert),
add CONSTRAINT _joue_groupe_concert_fk1 FOREIGN KEY (id_groupe) REFERENCES ATM._GROUPE(id),
add CONSTRAINT _joue_groupe_concert_fk2 FOREIGN KEY (id_concert) REFERENCES ATM._CONCERT(id);

CREATE TABLE ATM._DEROULE_VILLE_FESTIVAL (
 id_ville           INTEGER,
 id_festival           INTEGER
);
alter table ATM._DEROULE_VILLE_FESTIVAL
add CONSTRAINT _DEROULE_VILLE_FESTIVAL_pk PRIMARY KEY (id_ville,id_festival),
add CONSTRAINT _DEROULE_VILLE_FESTIVAL_fk1 FOREIGN KEY (id_ville) REFERENCES ATM._VILLE(id),
add CONSTRAINT _DEROULE_VILLE_FESTIVAL_fk2 FOREIGN KEY (id_festival) REFERENCES ATM._FESTIVAL(id);
