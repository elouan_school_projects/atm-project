SET SCHEMA 'ATM';

---CREATION DES TABLES TEMPORAIRES----
CREATE TABLE ATM._ville_festival (
 nom_ville                 VARCHAR(100),
 nom_festival               VARCHAR(100)
);

CREATE TABLE ATM._ville_groupe (
 nom_ville               VARCHAR(100),
 nom_groupe              VARCHAR(100)
);

CREATE TABLE ATM._artiste_groupe (
 nom_groupe              VARCHAR(100),
 nom_artiste             VARCHAR(100)
);

CREATE TABLE ATM._ville_pays (
 nom_ville              VARCHAR(100),
 nom_pays             VARCHAR(100)
);

CREATE TABLE ATM._ville_lieu (
 nom_ville              VARCHAR(100),
 nom_lieu             VARCHAR(100)
);

CREATE TABLE ATM._ville_artiste (
 nom_ville              VARCHAR(100),
 nom_artiste             VARCHAR(100)
);

CREATE TABLE ATM._artiste_date (
 nom_artiste              VARCHAR(100),
 nom_date             VARCHAR(100)
);

CREATE TABLE ATM._date_soiree (
 nom_date              VARCHAR(100),
 nom_soiree             VARCHAR(100)
);

CREATE TABLE ATM._groupe_date (
 nom_groupe              VARCHAR(100),
 nom_date             VARCHAR(100)
);

CREATE TABLE ATM._date_projet (
 nom_date              VARCHAR(100),
 nom_projet             VARCHAR(100)
);

CREATE TABLE ATM._date_festival (
 nom_date              VARCHAR(100),
 nom_festival             VARCHAR(100)
);

CREATE TABLE ATM._reservation_lieu (
 nom_reservation              VARCHAR(100),
 nom_lieu             VARCHAR(100)
);

CREATE TABLE ATM._soiree_projet (
 nom_soiree              VARCHAR(100),
 nom_projet             VARCHAR(100)
);

CREATE TABLE ATM._festival_projet (
 nom_festival              VARCHAR(100),
 nom_projet             VARCHAR(100)
);

CREATE TABLE ATM._festival_soiree (
 nom_festival              VARCHAR(100),
 nom_soiree             VARCHAR(100)
);

CREATE TABLE ATM._reservation_date (
 nom_reservation              VARCHAR(100),
 nom_date             VARCHAR(100)
);
----FIN CREATION TABLES TEMPORAIRES----


