SET SCHEMA 'ATM';

-- Création  des vues et peuplement --
select _ville.id as id_ville,_festival.id as id_festival from ATM._ville_festival
inner join ATM._festival 
  on ATM._ville_festival.nom_festival=ATM._festival.nom
inner join ATM._ville 
  on ATM._ville_festival.nom_ville=ATM._ville.nom;
  
--vue avec requete precedente
create view ATM.liaison_festival_ville as 
select _ville.id as id_ville,ATM._festival.id as id_festival from ATM._ville_festival
inner join ATM._festival 
  on ATM._ville_festival.nom_festival=ATM._festival.nom
inner join ATM._ville 
  on ATM._ville_festival.nom_ville=ATM._ville.nom;
  


--creation table finale

insert into ATM._DEROULE_VILLE_FESTIVAL select id_ville,id_festival from ATM.liaison_festival_ville;

---------------VILLE-GROUPE

-- Création requete pour joindre id--
select ATM._groupe.id as id_groupe,ATM._ville.id as id_ville from ATM._ville_groupe
inner join ATM._groupe 
  on ATM._ville_groupe.nom_groupe=ATM._groupe.nom
inner join ATM._ville 
  on ATM._ville_groupe.nom_ville=ATM._ville.nom;
  
--vue avec requete precedente
create view ATM.liaison_groupe_ville as 
select ATM._ville.id as id_ville,ATM._groupe.id as id_groupe  from ATM._ville_groupe
inner join ATM._groupe 
  on ATM._ville_groupe.nom_groupe=ATM._groupe.nom
inner join ATM._ville 
  on ATM._ville_groupe.nom_ville=ATM._ville.nom;
  

--creation table finale

insert into  ATM._ORIGINE_VILLE_GROUPE select * from ATM.liaison_groupe_ville;

-- alter table  _ORIGINE_VILLE_GROUPE 
-- add CONSTRAINT _origine_ville_groupe_pk PRIMARY KEY (id_groupe,id_ville),
--  add CONSTRAINT _origine_ville_groupe_fk1 FOREIGN KEY (id_groupe) REFERENCES _GROUPE(id),
--  add CONSTRAINT _origine_ville_groupe_fk2 FOREIGN KEY (id_ville) REFERENCES _VILLE(id);


--suppression des tables et vues temporaires


---------------ARTISTE-GROUPE

-- Création requete pour joindre id--
select ATM._groupe.id as id_groupe,ATM._artiste.id as id_artiste from ATM._artiste_groupe
inner join ATM._groupe 
  on ATM._artiste_groupe.nom_groupe=ATM._groupe.nom
inner join ATM._artiste 
  on ATM._artiste_groupe.nom_artiste=ATM._artiste.nom;
  
--vue avec requete precedente
create view ATM.liaison_artiste_groupe as 
select ATM._artiste.id as id_artiste,ATM._groupe.id as id_groupe from ATM._artiste_groupe
inner join ATM._groupe 
  on ATM._artiste_groupe.nom_groupe=ATM._groupe.nom
inner join ATM._artiste 
  on ATM._artiste_groupe.nom_artiste=ATM._artiste.nom;
 
--drop view ATM.liaison_artiste_groupe;



--creation table finale

insert into  ATM._MEMBRE_ARTISTE_GROUPE select * from ATM.liaison_artiste_groupe ;

---------------ville-pays
-- Création requete pour joindre id--
select ATM._ville.id as id_ville,ATM._pays.id as id_pays from ATM._ville_pays
inner join ATM._ville 
  on ATM._ville_pays.nom_ville=ATM._ville.nom
inner join ATM._pays 
  on ATM._ville_pays.nom_pays=ATM._pays.nom;
  
--vue avec requete precedente
create view ATM.liaison_pays_ville as 
select ATM._ville.id as id_ville,ATM._pays.id as id_pays from ATM._ville_pays
inner join ATM._ville 
  on ATM._ville_pays.nom_ville=ATM._ville.nom
inner join ATM._pays 
  on ATM._ville_pays.nom_pays=ATM._pays.nom;



--creation table finale

update ATM._VILLE  set pays_id = id_pays
from ATM.liaison_pays_ville
where ATM.liaison_pays_ville.id_ville=ATM._VILLE.id;

---------------ville-lieu

-- Création requete pour joindre id--
select ATM._ville.id as id_ville,ATM._lieu.id as id_lieu from ATM._ville_lieu
inner join ATM._ville 
  on ATM._ville_lieu.nom_ville=ATM._ville.nom
inner join ATM._lieu 
  on ATM._ville_lieu.nom_lieu=ATM._lieu.nom;
  
--vue avec requete precedente
create view ATM.liaison_lieu_ville as 
select ATM._ville.id as id_ville,ATM._lieu.id as id_lieu from ATM._ville_lieu
inner join ATM._ville 
  on ATM._ville_lieu.nom_ville=ATM._ville.nom
inner join ATM._lieu 
  on ATM._ville_lieu.nom_lieu=ATM._lieu.nom;



--creation table finale

update ATM._LIEU  set ville_id = id_ville
from ATM.liaison_lieu_ville
where ATM.liaison_lieu_ville.id_lieu=ATM._LIEU.id;

---------------ville-artiste
-- Création requete pour joindre id--
select ATM._ville.id as id_ville,ATM._artiste.id as id_artiste from ATM._ville_artiste
inner join ATM._ville 
  on ATM._ville_artiste.nom_ville=ATM._ville.nom
inner join ATM._artiste 
  on ATM._ville_artiste.nom_artiste=ATM._artiste.nom;
  
--vue avec requete precedente
create view ATM.liaison_artiste_ville as 
select ATM._ville.id as id_ville,ATM._artiste.id as id_artiste from ATM._ville_artiste
inner join ATM._ville 
  on ATM._ville_artiste.nom_ville=ATM._ville.nom
inner join ATM._artiste 
  on ATM._ville_artiste.nom_artiste=ATM._artiste.nom;

--creation table finale

update ATM._artiste  set ville_origine_id = id_ville
from ATM.liaison_artiste_ville
where ATM.liaison_artiste_ville.id_artiste=ATM._artiste.id;





---------------artiste-date
-- Création requete pour joindre id--
select ATM._artiste.id as id_artiste,ATM._date.id as id_date from ATM._artiste_date
inner join ATM._artiste 
  on ATM._artiste_date.nom_artiste=ATM._artiste.nom
inner join ATM._date 
  on ATM._artiste_date.nom_date=ATM._date.date::varchar;
  
--vue avec requete precedente
create view ATM.liaison_date_artiste as 
select ATM._artiste.id as id_artiste,ATM._date.id as id_date from ATM._artiste_date
inner join ATM._artiste 
  on ATM._artiste_date.nom_artiste=ATM._artiste.nom
inner join ATM._date 
  on ATM._artiste_date.nom_date=ATM._date.date::varchar;



--creation table finale

update ATM._artiste  set naissance_id = id_date
from ATM.liaison_date_artiste
where ATM.liaison_date_artiste.id_artiste=ATM._artiste.id;

---------------groupe-date
-- Création requete pour joindre id--
select ATM._groupe.id as id_groupe,ATM._date.id as id_date from ATM._groupe_date
inner join ATM._groupe 
  on ATM._groupe_date.nom_groupe=ATM._groupe.nom
inner join ATM._date 
  on ATM._groupe_date.nom_date=ATM._date.date::varchar;
  
--vue avec requete precedente
create view ATM.liaison_date_groupe as 
select ATM._groupe.id as id_groupe,ATM._date.id as id_date from ATM._groupe_date
inner join ATM._groupe 
  on ATM._groupe_date.nom_groupe=ATM._groupe.nom
inner join ATM._date 
  on ATM._groupe_date.nom_date=ATM._date.date::varchar;



--creation table finale

update ATM._groupe  set date_formation_id = id_date
from ATM.liaison_date_groupe
where ATM.liaison_date_groupe.id_groupe=ATM._groupe.id;



---------------date-soiree
-- Création requete pour joindre id--
select ATM._date.id as id_date,ATM._soiree.id as id_soiree from ATM._date_soiree
inner join ATM._date 
  on ATM._date_soiree.nom_date=ATM._date.date::varchar
inner join ATM._soiree 
  on ATM._date_soiree.nom_soiree=ATM._soiree.nom;
  
--vue avec requete precedente
create view ATM.liaison_soiree_date as 
select ATM._date.id as id_date,ATM._soiree.id as id_soiree from ATM._date_soiree
inner join ATM._date 
  on ATM._date_soiree.nom_date=ATM._date.date::varchar
inner join ATM._soiree 
  on ATM._date_soiree.nom_soiree=ATM._soiree.nom;

--creation table finale

update ATM._soiree  set date_deb_id = id_date
from ATM.liaison_soiree_date
where ATM.liaison_soiree_date.id_soiree=ATM._soiree.id;



---------------date-projet
-- Création requete pour joindre id--
select ATM._date.id as id_date,ATM._projet.id as id_projet from ATM._date_projet
inner join ATM._date 
  on ATM._date_projet.nom_date=ATM._date.date::varchar
inner join ATM._projet 
  on ATM._date_projet.nom_projet=ATM._projet.nom;
  
--vue avec requete precedente
create view ATM.liaison_projet_date as 
select ATM._date.id as id_date,_projet.id as id_projet from ATM._date_projet
inner join ATM._date 
  on ATM._date_projet.nom_date=ATM._date.date::varchar
inner join ATM._projet 
  on ATM._date_projet.nom_projet=ATM._projet.nom;



--creation table finale

update ATM._projet  set date_deb_id = id_date
from ATM.liaison_projet_date
where ATM.liaison_projet_date.id_projet=ATM._projet.id;



---------------date-festival
-- Création requete pour joindre id--
select ATM._date.id as id_date,_festival.id as id_festival from ATM._date_festival
inner join ATM._date 
  on ATM._date_festival.nom_date=ATM._date.date::varchar
inner join ATM._festival 
  on ATM._date_festival.nom_festival=ATM._festival.nom;
  
--vue avec requete precedente
create view ATM.liaison_festival_date as 
select ATM._date.id as id_date,ATM._festival.id as id_festival from ATM._date_festival
inner join ATM._date undefined
  on ATM._date_festival.nom_date=ATM._date.date::varchar
inner join ATM._festival 
  on ATM._date_festival.nom_festival=ATM._festival.nom;

--creation table finale

update ATM._festival  set date_deb_id = id_date
from ATM.liaison_festival_date
where ATM.liaison_festival_date.id_festival=ATM._festival.id;



---------------soiree-projet
-- Création requete pour joindre id--
select ATM._soiree.id as id_soiree,ATM._projet.id as id_projet from ATM._soiree_projet
inner join ATM._soiree 
  on ATM._soiree_projet.nom_soiree=ATM._soiree.nom
inner join ATM._projet 
  on ATM._soiree_projet.nom_projet=ATM._projet.nom;
  
--vue avec requete precedente
create view ATM.liaison_projet_soiree as 
select ATM._soiree.id as id_soiree,ATM._projet.id as id_projet from ATM._soiree_projet
inner join ATM._soiree 
  on ATM._soiree_projet.nom_soiree=ATM._soiree.nom
inner join ATM._projet 
  on ATM._soiree_projet.nom_projet=ATM._projet.nom;

--creation table finale

update ATM._soiree  set projet_id = id_projet
from ATM.liaison_projet_soiree
where ATM.liaison_projet_soiree.id_soiree=ATM._soiree.id;





---------------festival-projet
-- Création requete pour joindre id--
select ATM._festival.id as id_festival,ATM._projet.id as id_projet from ATM._festival_projet
inner join ATM._festival 
  on ATM._festival_projet.nom_festival=ATM._festival.nom
inner join ATM._projet 
  on ATM._festival_projet.nom_projet=ATM._projet.nom;
  
--vue avec requete precedente
create view ATM.liaison_projet_festival as 
select ATM._festival.id as id_festival,ATM._projet.id as id_projet from ATM._festival_projet
inner join ATM._festival 
  on ATM._festival_projet.nom_festival=ATM._festival.nom
inner join ATM._projet 
  on ATM._festival_projet.nom_projet=ATM._projet.nom;

--creation table finale

update ATM._projet  set festival_id = id_festival
from ATM.liaison_projet_festival
where ATM.liaison_projet_festival.id_projet=ATM._projet.id;




---------------festival-soiree
-- Création requete pour joindre id--
select ATM._festival.id as id_festival,ATM._soiree.id as id_soiree from ATM._festival_soiree
inner join ATM._festival 
  on ATM._festival_soiree.nom_festival=ATM._festival.nom
inner join ATM._soiree
  on ATM._festival_soiree.nom_soiree=ATM._soiree.nom;
  
--vue avec requete precedente
create view ATM.liaison_soiree_festival as 
select ATM._festival.id as id_festival,ATM._soiree.id as id_soiree from ATM._festival_soiree
inner join ATM._festival 
  on ATM._festival_soiree.nom_festival=ATM._festival.nom
inner join ATM._soiree 
  on ATM._festival_soiree.nom_soiree=ATM._soiree.nom;
--creation table finale

update ATM._soiree  set festival_id = id_festival
from ATM.liaison_soiree_festival
where ATM.liaison_soiree_festival.id_soiree=ATM._soiree.id;




---------------reservation-date
-- Création requete pour joindre id--
select ATM._reservation.id as id_reservation,ATM._date.id as id_date from ATM._reservation_date
inner join ATM._reservation 
  on ATM._reservation_date.nom_reservation=ATM._reservation.id::varchar
inner join ATM._date 
  on ATM._reservation_date.nom_date=ATM._date.date::varchar;
  
--vue avec requete precedente
create view ATM.liaison_date_reservation as 
select ATM._reservation.id as id_reservation,ATM._date.id as id_date from ATM._reservation_date
inner join ATM._reservation 
  on ATM._reservation_date.nom_reservation=ATM._reservation.id::varchar
inner join ATM._date 
  on ATM._reservation_date.nom_date=ATM._date.date::varchar;

--creation table finale
update ATM._reservation  set date_deb_id = id_date
from ATM.liaison_date_reservation
where ATM.liaison_date_reservation.id_reservation=ATM._reservation.id;



---------------reservation-lieu
-- Création requete pour joindre id--
select ATM._reservation.id as id_reservation,ATM._lieu.id as id_lieu from ATM._reservation_lieu
inner join ATM._reservation 
  on ATM._reservation_lieu.nom_reservation=ATM._reservation.id::varchar
inner join ATM._lieu 
  on ATM._reservation_lieu.nom_lieu=ATM._lieu.nom::varchar;
  
--vue avec requete precedente
create view ATM.liaison_lieu_reservation as 
select ATM._reservation.id as id_reservation,ATM._lieu.id as id_lieu from ATM._reservation_lieu
inner join ATM._reservation 
  on ATM._reservation_lieu.nom_reservation=ATM._reservation.id::varchar
inner join ATM._lieu 
  on ATM._reservation_lieu.nom_lieu=ATM._lieu.nom::varchar;



--creation table finale

update ATM._reservation  set lieu_id = id_lieu
from ATM.liaison_lieu_reservation
where ATM.liaison_lieu_reservation.id_reservation=ATM._reservation.id;







---DROP DES TABLES INUTILE---

--suppressiATM.on des tables et vues temporaires
drop table ATM._ville_festival cascade;
drop table ATM._ville_groupe cascade;
drop table ATM._artiste_groupe cascade;
drop table ATM._ville_pays cascade;
drop table ATM._ville_lieu cascade;
drop table ATM._ville_artiste cascade;
drop table ATM._artiste_date cascade;
drop table ATM._groupe_date cascade;
drop table ATM._date_soiree cascade;
drop table ATM._date_projet cascade;
drop table ATM._date_festival cascade;
drop table ATM._soiree_projet cascade;
drop table ATM._festival_projet cascade;
drop table ATM._festival_soiree cascade;
drop table ATM._reservation_date cascade;
drop table ATM._reservation_lieu cascade;
