DROP SCHEMA ATM CASCADE;
CREATE SCHEMA ATM;
SET SCHEMA 'ATM';

-- Création des 13 tables d'origine--
CREATE TABLE ATM._PAYS (
 id                SERIAL CONSTRAINT _pays_pk PRIMARY KEY,
 nom               VARCHAR(100)
);
CREATE TABLE ATM._VILLE (
 id                SERIAL CONSTRAINT _ville_pk PRIMARY KEY,
 nom               VARCHAR(100),
 pays_id           INTEGER,
 CONSTRAINT _ville_fk1 FOREIGN KEY (pays_id) REFERENCES ATM._PAYS(id)
);
CREATE TABLE ATM._LIEU_TYPE (
 id                SERIAL CONSTRAINT _lieu_type_pk PRIMARY KEY,
 description       VARCHAR(100)
);

CREATE TABLE ATM._LIEU (
 id                SERIAL CONSTRAINT _lieu_pk PRIMARY KEY,
 nom               VARCHAR(100),
 acces_handi       INTEGER,
 nombre_place      INTEGER,
 ville_id          INTEGER,
 type_lieu_id      INTEGER,
 CONSTRAINT _lieu_fk1 FOREIGN KEY (type_lieu_id) REFERENCES ATM._LIEU_TYPE(id),
 CONSTRAINT _lieu_fk2 FOREIGN KEY (ville_id) REFERENCES ATM._VILLE(id)
);
CREATE TABLE ATM._DATE (
 id                SERIAL CONSTRAINT _date_pk PRIMARY KEY,
 date              timestamptz
);
SET timezone = 'Europe/Paris';

CREATE TABLE ATM._GROUPE (
 id                SERIAL CONSTRAINT _groupe_pk PRIMARY KEY,
 nom               VARCHAR(100),
 genre             VARCHAR(100),
 date_formation_id INTEGER,
 CONSTRAINT _groupe_fk1 FOREIGN KEY (date_formation_id) REFERENCES ATM._DATE(id)
);

CREATE TABLE ATM._COMPTE (
 id                        SERIAL CONSTRAINT _compte_pk PRIMARY KEY,
 nom_utilisateur           VARCHAR(100),
 mot_de_passe              VARCHAR(100),
 mail                      VARCHAR(100),
 etat			                 INTEGER,
 niveau_droit              INTEGER,
 artiste_id                INTEGER
 
);
CREATE TABLE ATM._ARTISTE (
 id                SERIAL CONSTRAINT _artiste_pk PRIMARY KEY,
 nom               VARCHAR(100),
 ville_origine_id  INTEGER,
 naissance_id      INTEGER,
 compte_id         INTEGER,
 CONSTRAINT _artiste_fk1 FOREIGN KEY (ville_origine_id) REFERENCES ATM._VILLE(id),
 CONSTRAINT _artiste_fk2 FOREIGN KEY (naissance_id) REFERENCES ATM._DATE(id),
 constraint _artiste_fk3 FOREIGN KEY (compte_id) REFERENCES ATM._COMPTE(id)
);
 alter table ATM._COMPTE add CONSTRAINT _compte_fk1 FOREIGN KEY (artiste_id) REFERENCES ATM._ARTISTE(id);


CREATE TABLE ATM._FESTIVAL (
 id                SERIAL CONSTRAINT _festival_pk PRIMARY KEY,
 nom               VARCHAR(100),
 date_deb_id       INTEGER,
 date_fin_id       INTEGER,
 ville_id          INTEGER,
 CONSTRAINT _festival_fk1 FOREIGN KEY (date_deb_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _festival_fk2 FOREIGN KEY (date_fin_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _festival_fk3 FOREIGN KEY (ville_id) REFERENCES ATM._VILLE(id)
);

CREATE TABLE ATM._PROJET (
 id                SERIAL CONSTRAINT _projet_pk PRIMARY KEY,
 nom               VARCHAR(100),
 date_deb_id       INTEGER,
 date_fin_id       INTEGER,
 festival_id       INTEGER,
 CONSTRAINT _projet_fk1 FOREIGN KEY (date_deb_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _projet_fk2 FOREIGN KEY (date_fin_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _projet_fk3 FOREIGN KEY (festival_id) REFERENCES ATM._FESTIVAL(id)

);

CREATE TABLE ATM._SOIREE (
 id                SERIAL CONSTRAINT _soiree_pk PRIMARY KEY,
 nom               VARCHAR(100),
 duree             INTEGER,
 date_deb_id       INTEGER,
 date_fin_id       INTEGER,
 projet_id         INTEGER,
 festival_id       INTEGER,
 CONSTRAINT _soiree_fk1 FOREIGN KEY (date_deb_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _soiree_fk2 FOREIGN KEY (date_fin_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _soiree_fk3 FOREIGN KEY (projet_id) REFERENCES ATM._PROJET(id),
 CONSTRAINT _soiree_fk4 FOREIGN KEY (festival_id) REFERENCES ATM._FESTIVAL(id)
);
CREATE TABLE ATM._CONCERT (
 id                SERIAL CONSTRAINT _concert_pk PRIMARY KEY,
 nom               VARCHAR(100),
 duree             INTEGER
);
CREATE TABLE ATM._RESERVATION (
 id                SERIAL CONSTRAINT _reservation_pk PRIMARY KEY,
 etat              INTEGER,
 concert_id        INTEGER,
 date_deb_id       INTEGER,
 date_fin_id       INTEGER,
 lieu_id           INTEGER,
 projet_id         INTEGER,
 soiree_id         INTEGER,
 CONSTRAINT _reservation_fk1 FOREIGN KEY (concert_id) REFERENCES ATM._RESERVATION(id),
 CONSTRAINT _reservation_fk2 FOREIGN KEY (date_deb_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _reservation_fk3 FOREIGN KEY (date_fin_id) REFERENCES ATM._DATE(id),
 CONSTRAINT _reservation_fk4 FOREIGN KEY (lieu_id) REFERENCES ATM._LIEU(id),
 CONSTRAINT _reservation_fk5 FOREIGN KEY (projet_id) REFERENCES ATM._PROJET(id),
 CONSTRAINT _reservation_fk6 FOREIGN KEY (soiree_id) REFERENCES ATM._SOIREE(id)
);
