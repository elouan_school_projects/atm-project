SET SCHEMA 'ATM';

--IMPORT des tables de bases--

COPY ATM._artiste (id,nom)
FROM 'C:/Tables_objet_ARTISTE.csv' DELIMITER ',' CSV HEADER;

COPY ATM._concert (id,nom,duree)
FROM './CSV/Tables_objet_CONCERT.csv' DELIMITER ',' CSV HEADER;

COPY ATM._date (id,date)
FROM './CSV/Tables_objet_DATE.csv' DELIMITER ',' CSV HEADER;

COPY ATM._festival (id,nom)
FROM './CSV/Tables_objet_festival.csv' DELIMITER ',' CSV HEADER;

COPY ATM._groupe (id,nom,genre)
FROM './CSV/Tables_objet_GROUPE.csv' DELIMITER ',' CSV HEADER;

COPY ATM._lieu (id,nom,acces_handi,nombre_place)
FROM './CSV/Tables_objet_LIEU.csv' DELIMITER ',' CSV HEADER;

COPY ATM._lieu_type (id,description)
FROM './CSV/Tables_objet_TYPELIEU.csv' DELIMITER ',' CSV HEADER;


COPY ATM. _pays (id,nom)
FROM './CSV/Tables_objet_PAYS.csv' DELIMITER ',' CSV HEADER;


COPY ATM._projet (id,nom)
FROM './CSV/Tables_objet_PROJET.csv' DELIMITER ',' CSV HEADER;


COPY ATM._reservation (id,etat)
FROM './CSV/Tables_objet_RESERVATION.csv' DELIMITER ',' CSV HEADER;


COPY ATM._soiree (id,nom,duree)
FROM './CSV/Tables_objet_SOIREE.csv' DELIMITER ',' CSV HEADER;


COPY ATM._ville (id,date)
FROM './CSV/Tables_objet_VILLE.csv' DELIMITER ',' CSV HEADER;
 
 
 --IMPORT des tables de liaisons--

COPY ATM._ville_festival (nom_ville,nom_festival)
FROM './CSV/Tables_liaison_ville_festival.csv' DELIMITER ',' CSV HEADER;

COPY ATM._ville_groupe (nom_ville,nom_groupe)
FROM './CSV/Tables_liaison_ville_groupe.csv' DELIMITER ',' CSV HEADER;

COPY ATM._artiste_groupe (nom_artiste,nom_groupe)
FROM './CSV/Tables_liaison_artiste_groupe.csv' DELIMITER ',' CSV HEADER;

COPY ATM._ville_pays (nom_ville,nom_pays)
FROM './CSV/Tables_liaison_ville_pays.csv' DELIMITER ',' CSV HEADER;

COPY ATM._ville_lieu (nom_ville,nom_lieu)
FROM './CSV/Tables_liaison_ville_lieu.csv' DELIMITER ',' CSV HEADER;

COPY ATM._ville_artiste (nom_ville,nom_artiste)
FROM './CSV/Tables_liaison_ville_artiste.csv' DELIMITER ',' CSV HEADER;

COPY ATM._artiste_date (nom_artiste,nom_date)
FROM './CSV/Tables_liaison_artiste_date.csv' DELIMITER ',' CSV HEADER;

COPY ATM._date_soiree (nom_date,nom_soiree)
FROM './CSV/Tables_liaison_date_soiree.csv' DELIMITER ',' CSV HEADER;

COPY ATM._groupe_date (nom_groupe,nom_date)
FROM './CSV/Tables_liaison_groupe_soiree.csv' DELIMITER ',' CSV HEADER;

COPY ATM._date_projet (nom_date,nom_projet)
FROM './CSV/Tables_liaison_date_projet.csv' DELIMITER ',' CSV HEADER;

COPY ATM._date_festival (nom_date,nom_festival)
FROM './CSV/Tables_liaison_date_festival.csv' DELIMITER ',' CSV HEADER;

COPY ATM._reservation_lieu (nom_reservation,nom_lieu)
FROM './CSV/Tables_liaison_reservation_lieu.csv' DELIMITER ',' CSV HEADER;

COPY ATM._soiree_projet (nom_soiree,nom_projet)
FROM './CSV/Tables_liaison_soiree_projet.csv' DELIMITER ',' CSV HEADER;

COPY ATM._festival_projet (nom_festival,nom_projet)
FROM './CSV/Tables_liaison_festival_projet.csv' DELIMITER ',' CSV HEADER;

COPY ATM._festival_soiree (nom_date,nom_projet)
FROM './CSV/Tables_liaison_festival_soiree.csv' DELIMITER ',' CSV HEADER;

COPY ATM._reservation_date (nom_reservation,nom_date)
FROM './CSV/Tables_liaison_reservation_date.csv' DELIMITER ',' CSV HEADER;
